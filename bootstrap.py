#!/usr/bin/env python3
from lib.jaklis.lib.natools import get_privkey
import secrets
from duniterpy.documents.identity import Identity
from duniterpy.documents.membership import Membership
from duniterpy.documents.certification import Certification
from duniterpy.documents.block import Block
from duniterpy.documents.block_uid import BlockUID, block_uid
from duniterpy.key.signing_key import SigningKey


class bootstrapChain():

    def generateMembers(self, currency, nbrMembers):
    
        sigkey_list =  [duniterpy.key.SigningKey.from_seedhex(secrets.token_bytes(32).hex()) for i in range(nbrMembers)]
        # print(sigkey_list)
        
        for (i, key) in enumerate(sigkey_list):
            # use member key
            ### identity ###
            # see https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md#identity
            member_idty = Identity (
                version = 10,
                currency = currency,
                pubkey = key.pubkey,
                uid = str(i),
                ts = special_block_uid,
                signature = "",
            )
            member_idty.sign([key])

            identities.append(member_idty)

            ### membership ###
            # see https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#membership
            membership = Membership(
                version = 10,
                currency = currency,
                issuer = member["pubkey"],
                membership_ts = special_block_uid,
                membership_type = "IN",
                uid = member["id"],
                identity_ts = special_block_uid,
                signature = "",
            )
            membership.sign([key])

            memberships.append(membership)

            ### certifications ###
            # certifications should be sent _after_ each member has sent its identity and membership.
            # see https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#certification

        for m, member in enumerate(members):
            # use member key
            key = SigningKey.from_seedhex(member["seed"])
            for i, identity in enumerate(identities):
                # no auto-certification
                if m == i:
                pass
                else:
                cert = Certification(
                    version = 10,
                    currency = currency,
                    pubkey_from = member["pubkey"],
                    identity = identities[i],
                    timestamp = special_block_uid,
                    signature = None,
                )
                cert.sign([key])

                certifications.append(cert)











    generateMembers()



