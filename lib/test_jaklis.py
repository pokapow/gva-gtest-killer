#!/usr/bin/env python3.9

import os
from os.path import dirname, join
from shutil import copyfile
import sys

from dotenv.main import load_dotenv
from lib.gva import GvaApi

MY_PATH = os.path.realpath(os.path.dirname(sys.argv[0])) + '/'

# Get variables environment
if not os.path.isfile(MY_PATH + '.env'):
    copyfile(MY_PATH + ".env.template",MY_PATH +  ".env")
dotenv_path = join(dirname(__file__),MY_PATH +  '.env')
load_dotenv(dotenv_path)

dunikey = os.getenv('DUNIKEY')
if not os.path.isfile(dunikey):
    HOME = os.getenv("HOME")
    dunikey = HOME + dunikey
    if not os.path.isfile(dunikey):
        sys.stderr.write('Le fichier de trousseau {0} est introuvable.\n'.format(dunikey))
        sys.exit(1)
        
node = os.getenv('NODE')
            

gva = GvaApi(dunikey, node, "2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ", False)

def getHistory():
    print("yeah")
    gva.history(False, False, 20)
    
    

getHistory()